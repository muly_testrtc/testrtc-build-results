#! /bin/bash

set -e

echo "*Pull Media Convert mulyoved/media-convert:v1.8.1"
docker pull mulyoved/media-convert:v1.8.1

echo "*Pull Node chrome mulyoved/node-chrome-stable:v1.8.1"
docker pull mulyoved/node-chrome-stable:v1.8.1

echo "*Pull Standalone chrome mulyoved/chrome-stable:v1.8.1"
docker pull mulyoved/chrome-stable:v1.8.1

echo "*Pull Standalone Debug chrome mulyoved/chrome-debug-stable:v1.8.1"
docker pull mulyoved/chrome-debug-stable:v1.8.1

echo "*Pull Node chrome mulyoved/node-chrome-unstable:v1.8.1"
docker pull mulyoved/node-chrome-unstable:v1.8.1

echo "*Pull Standalone chrome mulyoved/chrome-unstable:v1.8.1"
docker pull mulyoved/chrome-unstable:v1.8.1

echo "*Pull Standalone Debug chrome mulyoved/chrome-debug-unstable:v1.8.1"
docker pull mulyoved/chrome-debug-unstable:v1.8.1

echo "*Pull Node chrome mulyoved/node-chrome-beta:v1.8.1"
docker pull mulyoved/node-chrome-beta:v1.8.1

echo "*Pull Standalone chrome mulyoved/chrome-beta:v1.8.1"
docker pull mulyoved/chrome-beta:v1.8.1

echo "*Pull Standalone Debug chrome mulyoved/chrome-debug-beta:v1.8.1"
docker pull mulyoved/chrome-debug-beta:v1.8.1

echo "*Pull Node chrome mulyoved/node-chrome-v43:v1.8.1"
docker pull mulyoved/node-chrome-v43:v1.8.1

echo "*Pull Standalone chrome mulyoved/chrome-v43:v1.8.1"
docker pull mulyoved/chrome-v43:v1.8.1

echo "*Pull Standalone Debug chrome mulyoved/chrome-debug-v43:v1.8.1"
docker pull mulyoved/chrome-debug-v43:v1.8.1

echo "*Pull Node chrome mulyoved/node-chrome-v42:v1.8.1"
docker pull mulyoved/node-chrome-v42:v1.8.1

echo "*Pull Standalone chrome mulyoved/chrome-v42:v1.8.1"
docker pull mulyoved/chrome-v42:v1.8.1

echo "*Pull Standalone Debug chrome mulyoved/chrome-debug-v42:v1.8.1"
docker pull mulyoved/chrome-debug-v42:v1.8.1

echo "*Pull Node firefox mulyoved/node-firefox-stable:v1.8.1"
docker pull mulyoved/node-firefox-stable:v1.8.1

echo "*Pull Standalone firefox mulyoved/firefox-stable:v1.8.1"
docker pull mulyoved/firefox-stable:v1.8.1

echo "*Pull Standalone Debug firefox mulyoved/firefox-debug-stable:v1.8.1"
docker pull mulyoved/firefox-debug-stable:v1.8.1
